import MeetUp from '../models/Meetup.js'


async listar( params ) {
    let urlMeetup = params.urlMeetupPreenchida || `${ this.url }/${ params.idMeetup}/`

    return new Promise (resolve => {
        fetch( urlMeetup )
        .then(j => j.json() )
        .then(p => {
            const meetUp = new MeetUp (m)
            resolve(meetUp)
        })
    })
}