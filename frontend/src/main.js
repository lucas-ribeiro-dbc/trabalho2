import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import VeeValidate, { Validator } from 'vee-validate'
import ptBR from 'vee-validate/dist/locale/pt_BR'
import Login from './components/screens/Login.vue'

import TelaPrincipal from './components/screens/TelaPrincipal.vue'
import MeusJogos from './components/screens/MeusJogos.vue'

Validator.localize('pt_BR', ptBR)
Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VeeValidate)

const routes = [
  { name: 'principal', path: '/principal', component: TelaPrincipal },
  { name: 'login', path: '/', component: Login },
  { name: 'meusJogos', path: '/meusJogos', component: MeusJogos },

]

const router = new VueRouter({
  routes,
 mode: "history"
});

new Vue( {
  router,
  render: h => h(App)
}).$mount('#app')

