package com.dbc.games;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AppRunner implements CommandLineRunner {

    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Override
    @Transactional(readOnly = false)
    public void run(String... args) throws Exception {        
        System.out.println("Salva dados básicos");     
                
        jdbcTemplate.update("INSERT INTO USUARIO (ID, USERNAME, NOME,SOBRENOME, PASSWORD) "
                + "VALUES (1, 'admin.admin', 'admin', 'admin', 'admin')");
        
        jdbcTemplate.update("INSERT INTO JOGO (ID, TITULO, CATEGORIA, PRECO) VALUES (1, 'Bomberman', 'Estratégia', 2.50)");
        jdbcTemplate.update("INSERT INTO JOGO (ID, TITULO, CATEGORIA, PRECO) VALUES (2, 'Bomberman 2', 'Estratégia', 3.50)");
        jdbcTemplate.update("INSERT INTO JOGO (ID, TITULO, CATEGORIA, PRECO) VALUES (3, 'Bomberman 3', 'Estratégia', 4.50)");
        jdbcTemplate.update("INSERT INTO JOGO (ID, TITULO, CATEGORIA, PRECO) VALUES (4, 'Bomberman 4', 'Estratégia', 5.50)");
        jdbcTemplate.update("INSERT INTO JOGO (ID, TITULO, CATEGORIA, PRECO) VALUES (5, 'Bomberman 5', 'Estratégia', 6.50)");
        
        System.out.println("salvou dados");
    }   
}