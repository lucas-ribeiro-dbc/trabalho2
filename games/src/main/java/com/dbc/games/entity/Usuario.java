package com.dbc.games.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USUARIO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name="USUARIO_SEQ", initialValue = 1, allocationSize = 1)
public class Usuario extends AbstractEntity implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USUARIO_SEQ")
    private Long id;
    
    @NotNull
    @Column(name = "USERNAME")
    private String username;
    @NotNull
    @Column(name = "NOME")
    private String primeiroNome;
    @Column(name = "SOBRENOME")
    private String sobrenome;
    @Column(name = "PASSWORD")
    private String password;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JsonIgnore
    private List<Jogo> jogosList;
     
}