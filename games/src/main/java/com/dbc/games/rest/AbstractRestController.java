package com.dbc.games.rest;

import com.dbc.games.entity.AbstractEntity;
import com.dbc.games.service.AbstractService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

public abstract class AbstractRestController<E extends AbstractEntity> {
    
    @Autowired
    public abstract AbstractService<E> getService();

    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(this.getService().findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable Long id) {
        return this.getService().findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody E entity) {
        if (id == null || !Objects.equals(entity.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(this.getService().save(entity));
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody E entity) {
        if (entity.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(this.getService().save(entity));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        this.getService().delete(id);
        return ResponseEntity.noContent().build();
    }

}
