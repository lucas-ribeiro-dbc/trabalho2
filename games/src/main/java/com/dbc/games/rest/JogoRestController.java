package com.dbc.games.rest;

import com.dbc.games.entity.Jogo;
import com.dbc.games.service.AbstractService;
import com.dbc.games.service.JogoService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/api/jogo")
public class JogoRestController extends AbstractRestController<Jogo> {

    @Autowired
    private JogoService jogoService;

    public AbstractService<Jogo> getService() {//Generic recebe ClienteService, que extende AbstractService
        return this.jogoService;
    }

}
