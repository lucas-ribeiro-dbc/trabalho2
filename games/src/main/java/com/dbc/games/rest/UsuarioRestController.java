package com.dbc.games.rest;

import com.dbc.dto.CompraDTO;
import com.dbc.games.entity.Usuario;
import com.dbc.games.service.AbstractService;
import com.dbc.games.service.UsuarioService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioRestController extends AbstractRestController<Usuario> {

    @Autowired
    private UsuarioService usuarioService;

    public AbstractService<Usuario> getService() {//Generic recebe ClienteService, que extende AbstractService
        return this.usuarioService;
    }

    @GetMapping("username/{username}")
    public ResponseEntity<?> findByUsername(@PathVariable String username) {
        Usuario usuario = this.usuarioService.findByUsername(username);
        if (usuario == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(usuario);
    }
    @GetMapping("/meusjogos/{id}")
    public ResponseEntity<?> meusJogos(@PathVariable Long id) {
        Usuario usuario = this.usuarioService.findById(id).get();
        if (usuario == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(usuarioService.meusJogos(usuario));
    }
    @PostMapping("/comprar")
    public ResponseEntity<?> comprarJogo(@RequestBody CompraDTO compraDTO) {
        return ResponseEntity.ok(this.usuarioService.comprarJogo(compraDTO));
    }

}
