package com.dbc.games.service;
import com.dbc.games.entity.Jogo;
import com.dbc.games.repository.JogoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class JogoService extends AbstractService<Jogo> {
    
    @Autowired
    private JogoRepository jogoRepository;

    @Override
    public JpaRepository<Jogo, Long> getRepository() {
        return this.jogoRepository;
    }
}
