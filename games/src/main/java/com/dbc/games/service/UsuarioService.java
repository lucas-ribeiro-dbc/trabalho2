package com.dbc.games.service;

import com.dbc.dto.CompraDTO;
import com.dbc.games.entity.Jogo;
import com.dbc.games.entity.Usuario;
import com.dbc.games.repository.UsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService extends AbstractService<Usuario> {

    @Autowired
    private UsuarioRepository userRepository;
    
    @Autowired
    private JogoService jogoService;

    @Override
    public JpaRepository<Usuario, Long> getRepository() {
        return this.userRepository;
    }

    public Usuario findByUsername(String username){
         return this.userRepository.findByUsername(username);
 
    }
    public List<Jogo> meusJogos(Usuario user){
        return user.getJogosList();
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public CompraDTO comprarJogo(CompraDTO compraDTO){
        Usuario user = (getRepository().findById(compraDTO.getIdUsuario())).get();
        Jogo game = (jogoService.findById(compraDTO.getIdJogo())).get();
        
        user.getJogosList().add(game);
        
        getRepository().save(user);
                
        return compraDTO;
    }
}
